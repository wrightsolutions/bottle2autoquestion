#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from bottle import abort, error, get, post, redirect, response, request
from bottle import route, run, static_file, template
from collections import defaultdict,namedtuple,OrderedDict
from os import linesep,path
import re
from string import printable
from time import sleep

SET_PRINTABLE = set(printable)
DIR_UPLOAD_UNEXPANDED = '~/j2upload'
""" Above UNEXPANDED is what will be presented in any feedback to prevent
unnecessary leakage of local path information
"""
DIR_UPLOAD = path.expanduser(DIR_UPLOAD_UNEXPANDED)
BYTES_MINIMUM = 5
RE3GROUP = re.compile('({{)(\s+\w*\s+)(}})')
# Next could be named REJ2COMMENT if you prefer, but {# something #} is referred to as disable
""" The following re looks like it should work, but instead I used '({#\s+)([ A-Za-z0-9_]*).*(#)(})'
REJ2DISABLE = re.compile('(\s*{#\s+)(\w*\s+)(#}\.*)')
"""
REJ2DISABLE_ALL = re.compile('({#\s+)(.+?)(#})')
REJ2DISABLE_ITER = re.compile('({#\s+)([ A-Za-z0-9_]*)(.*?)(#})')
""" Next template is for internal use and has a {0} placeholder that will be target of
a later substitution
"""
FORM_UPLOAD_TEMPLATE = """
        <form action="/{0}" method="post" enctype="multipart/form-data">
          <input type="text" name="port" />
          <input type="file" name="data" />
          <input type="submit" name="submit" value="Upload Template" />
        </form>
"""
FORM_ACTION_LINE = '<form action="/{0}" method="post" enctype="multipart/form-data">'
FORM_INPUT_LABEL_ZERO_ID = '<label for="{0}" name="{1}">{2}:</label>'
FORM_INPUT_LABEL_ZERO_ID4 = '<label for="{0}" name="{1}">{2}:</label><!-- {3} -->'
FORM_INPUT_ZERO_ID = '<input type="text" name="{0}" />'
FORM_INPUT_ZERO_ID_BR = '<input type="text" name="{0}" /><br />'
FORM_SUBMIT_BUTTON = '<input type="submit" name="submit" value="{0}" />'
HTML_BODY_WRAPPER = "<html><body>{0}</body></html>"
HTML_BODY_WRAPPER_NL = """<html><body>
{0}
</body></html>
"""

template_data = None
template_lines = []
template_dict = {}
template_numbered_dict = {}
template_field_count = 0
template_comment_namedtuples = []
template_newlines = None
template_filepath = None
template_comment_linenum_list = []
template_prompt_remove_list = []
done_dict = {}

TemplateField = namedtuple('TemplateField', ['index','linenum','linefieldnum','name'])
""" Next could alternatively be labelled TemplateDisabled as it stores things that
have been re matched using REJ2DISABLE. However TemplateComment seems a better fit to me """
TemplateComment = namedtuple('TemplateComment', ['index','linenum','text'])
LINE_COMMENT_WITH_NUMBER_P = '<p>Line {0} comment: {1}</p>'

@route('/')
@route('/index.html')
def index():
    return '<a href="/upload">Clickable link</a> to upload page'

@route('/hello')
def hello():
    return '<h1>hello nobody in particular</h1>'

@route('/hello/:name')
def hello_name(name):
    page = request.GET.get('page', '1')
    return "<h1>hello {0} <br/>({1})</h1>".format(name, page)

@route('/static/:filename')
def serve_static(filename):
    return static_file(filename, root=DIR_UPLOAD)

@route('/raise_error')
def raise_error():
    abort(404, "Aborted four oh four!")

@route('/redirect')
def redirect_to_hello():
    redirect('/hello')

@route('/ajax')
def ajax_response():
    return {'dictionary': 'Seeing ajax response? Content-Type will be "application/json"'}

@error(404)
def error404(error):
    return '404 error!'


def float2ints(float_to_split):
    """ There is no try except wrapping in here so caller should
    catch if it requires error trapping.
    """
    part1,part2 = divmod(float_to_split,1)
    part1int = int(part1)
    part2array = str(part2).split(chr(46))
    part2int = int(part2array[1])
    return (part1int,part2int)


def tuple4list(field_name=None):
    """ Generate a 4 tuple that can be used to associate a field with an optional prompt. 
    For convenience I repeat the definition of TemplateComment next:
    TemplateComment = namedtuple('TemplateComment', ['index','linenum','text'])
    """
    tuple4 = (None,None,None,None)
    tuple4list = []
    for idx,dnum in enumerate(sorted(template_numbered_dict)):
        fname = template_numbered_dict[dnum]
        linenum,linefieldnum = float2ints(dnum)
        linenum1 = linenum - 1
        field_prompt = None
        prompts = [ com.text for com in template_comment_namedtuples if com.linenum == linenum1 ]
        if len(prompts) == 1:
            field_prompt = prompts[0]
        else:
            try:
                field_prompt = prompts[linefieldnum]
            except:
                pass
        tuple4 = (fname,linenum,linefieldnum,field_prompt)
        if field_name is None:
            tuple4list.append(tuple4)
        elif field_name == fname:
            tuple4list.append(tuple4)
        else:
            pass
        tuple4 = (None,None,None,None)

    return tuple4list


@route('/zeroindexed')
def template_numbered_dict_bothfixed(prefix=None,suffix=None,uniq=False):
    bothfixed = ['{']
    field_name_set = set()

    html_flag = False
    if 'zeroindexed' in request['bottle.route'].rule and prefix is None and suffix is None:
        html_flag = True

    for idx,dnum in enumerate(sorted(template_numbered_dict)):
        field_name = template_numbered_dict[dnum]
        linenum,linefieldnum = float2ints(dnum)
        #linenum = template_dict[field_name][0]
        #linefieldnum = template_dict[field_name][1]
        if uniq is True and field_name in field_name_set:
            continue
        if html_flag is True:
            bothfixed.append("Field named '{0}': line {1} occurrence {2}".format(
                    field_name,linenum,linefieldnum))
        else:
            bothfixed.append("<p>Field named '{0}': line {1} occurrence {2}</p>".format(
                    field_name,linenum,linefieldnum))
        field_name_set.add(field_name)

    bothfixed.append('}')

    dict_repr = (linesep).join(bothfixed)

    if suffix is not None:
        dict_repr = "{0} {1}".format(dict_repr,suffix)
    if prefix is not None:
        dict_repr = "{1} {0}".format(dict_repr,prefix)

    if html_flag is True:
        bothfixed_raw_or_html = HTML_BODY_WRAPPER_NL.format(dict_repr)
    else:
        bothfixed_raw_or_html = dict_repr

    return bothfixed_raw_or_html


def fields_namedtuple_list(sort=True,uniq=False):
    """ Return a list of namedtuples()
    For convenience I repeat the definition of TemplateField which has 4 fields
    first of which is a simple index (zero-based)
    TemplateField = namedtuple('TemplateField', ['index','linenum','linefieldnum','name'])
    """
    nt_list = []
    field_name_set = set()
    for idx,dnum in enumerate(sorted(template_numbered_dict)):
        field_name = template_numbered_dict[dnum]
        linenum,linefieldnum = float2ints(dnum)
        #linenum = template_dict[field_name][0]
        #linefieldnum = template_dict[field_name][1]
        if uniq is True and field_name in field_name_set:
            continue
        nt_list.append(TemplateField(idx,linenum,linefieldnum,field_name))
        field_name_set.add(field_name)
    return nt_list


def fields_list(sort=True,uniq=False):
    flist = [ field.name for field in fields_namedtuple_list(sort,uniq) ] 
    """
    if uniq is True:
        before_list = fields_namedtuple_list(sort)
        flist = [ field.name for field in list(OrderedDict.fromkeys(before_list)) ]
    """
    return flist


def filename_postprocessed(filename):
    fname_pp = []
    filename_unicode = filename.encode('utf8','ignore')
    fname_ascii = filename_unicode.encode('ascii','ignore')
    if not set(fname_ascii).issubset(SET_PRINTABLE):
        return fname_pp
    fname_len = len(fname_ascii)
    if fname_len < 2:
        return fname_pp
    if '}' in fname_ascii and ';' in fname_ascii:
        return fname_pp
    #from collections import Counter
    dots = fname_ascii.count(chr(46))
    if dots > 1 and '..' in fname_ascii:
        return fname_pp
    if '%2' in fname_ascii:
        return fname_pp
    if 'chr(' in fname_ascii:
        return fname_pp
    fname_pp = fname_ascii
    return fname_pp


def upload_jinja2(fdata,fname,fbytes,fpart1,fext,filelinesep=linesep):

    global template_data, template_lines, template_dict, template_numbered_dict
    global template_field_count, template_filepath
    global template_comment_namedtuples, template_comment_linenum_list

    template_data = fdata
    written_bytes = -1
    template_filepath = "{0}/{1}".format(DIR_UPLOAD,fname)
    template_lines = []
    template_dict = {}
    template_numbered_dict = {}
    template_field_count = 0
    #template_comment_namedtuples = []
    #template_comment_linenum_list = []
    # Next is the index of comments / disabled
    cidx = 0
    try:
        with open(template_filepath, 'wt') as outf:
            for idx,line in enumerate(fdata.split(filelinesep)):

                template_lines.append(line)
                written_bytes = fbytes
                outf.write("{0}{1}".format(line,linesep))

                field_index = 0
                for mat in RE3GROUP.finditer(line):
                    template_field_count += 1
                    field_name = mat.group(2).strip()
                    num_string = "{0:d}.{1:d}".format(idx,field_index)
                    num = -1
                    try:
                        num = float(num_string)
                    except:
                        num = 0
                    if num > -1:
                        #print "Added an entry to template_numbered_dict!"
                        template_numbered_dict[num] = field_name
                        template_dict[field_name] = [idx,field_index]
                        field_index += 1

                if 0 == field_index:
                    # We were not a {{ something }} expression as field_index was not incremented
                    for cmat in REJ2DISABLE_ALL.findall(line):
                    #for cmat in REJ2DISABLE_ITER.findall(line):
                        """ We also appear to be a Jinja2 disabled block.
                        Store it as we might reference it
                        as a prompt for an autogenerated question later.
                        TemplateComment = namedtuple('TemplateComment', ['index','linenum','text'])
                        """
                        ctext = None
                        try:
                            cmiddle = cmat[1]
                            if set(cmiddle).issubset(SET_PRINTABLE):
                                ctext = cmiddle.lstrip()
                            #Above we have not removed the right hand spacing allowing flexibility in prompts
                        except:
                            print("Exception during extraction of Jinja2 disabled block.")

                        if ctext is not None:
                            template_comment_namedtuples.append(
                                TemplateComment(cidx,idx,ctext))
                            template_comment_linenum_list.append(idx)
                            cidx += 1

            written_bytes = fbytes

    except IOError:
        print "IOError. Does {0} directory exist?".format(DIR_UPLOAD_UNEXPANDED)
        written_bytes = 0
    except:
        print "Error during upload to directory {0}".format(DIR_UPLOAD_UNEXPANDED)
        raise
        written_bytes = 0

    return written_bytes


def feedback_of_fields():
    """ Build a string of lines reporting on field information.
    For convenience I repeat the definition of TemplateField which has 4 fields
    first of which is a simple index (zero-based)
    TemplateField = namedtuple('TemplateField', ['index','linenum','linefieldnum','name'])
    """
    fields_named_list = fields_namedtuple_list(True)
    feedback_list = []
    for field in fields_named_list:
        fline = "Field {0} on line {1} is field {2} for that line (with index={3}).".format(
            field.name,field.linenum,field.linefieldnum,field.index)
        feedback_list.append(fline)
    return feedback_list


def feedback_of_fields_newlined():
    """ wrapper around feedback of fields which gives list which we then join. """
    return (linesep).join(feedback_of_fields())


def feedback_of_fields_br(newline_replacement='<br />'):
    """ wrapper around feedback of fields which replaces newlines with
    the newline_replacement.
    """
    feedback = feedback_of_fields_newlined()
    feedback_br = feedback.replace(linesep,newline_replacement)
    return feedback_br


def feedback_of_fields_paragraphed():
    """ wrapper around feedback of fields which splits on newline
    and uses html paragraphing instead.
    """
    feedback_list = feedback_of_fields()
    feedback_p = (''.join('<p>' + item + '</p>' for item in feedback_list))
    return feedback_p


def field_prompt(field,default='field'):
    """ Reminder of the data structures by way of pseudocode / description of the task.
    We want the line number for a given field name and so consult
    namedtuples of type TemplateField generated by function fields_namedtuple_list()
    which is accessed by a function call rather than being permanently stored in a global.

    Alternatively we could go the more direct route and consult template_numbered_dict which
    is keyed on field name and has a list value like this [linenum,field_index] meaning
    the 2nd field in the 10th line would be stored as the list [9,1] and if our
    field was named 'diva_text' would have been assigned as shown next:
    template_numbered_dict['diva_text']=[9,1]

    But the problem with dictionaries keyed on name is that where we have repetition,
    the [linenum,fieldindex] would be that of the last occurence.

    So we generally do something like the following if we want high fidelity
        field_name = template_numbered_dict[dnum]
        linenum,linefieldnum = float2ints(dnum)

    """
    # tuple4 = (fname,linenum,linefieldnum,field_prompt)
    tup4list = tuple4list(field)
    if len(tup4list) == 1:
        prompt = tup4list[0][3]
        if default == 'field' and prompt is None:
            prompt = field
    return prompt


def upload_returning_feedback(fdata,fname,upload_bytes,fdata_raw,
                              verbosity=1):
    global done_dict
    global template_newlines
    if upload_bytes:
        part1, ext = path.splitext(fname)
        if ext == '.j2':
            if upload_bytes > BYTES_MINIMUM:
                done_bytes = upload_jinja2(fdata_raw,fname,upload_bytes,part1,ext)
                if done_bytes > BYTES_MINIMUM:
                    upload_feedback = "Successful upload of {0:d} bytes of file {1}".format(
                        done_bytes,fname)
                    if verbosity > 0:
                        upload_feedback = "{0}. Template has {1} lines and {2} variables".format(
                            upload_feedback,len(template_lines),template_field_count)
                    if verbosity > 1:
                        upload_feedback = "{0}.{1}{2}".format(
                            upload_feedback,linesep,feedback_of_fields_paragraphed())
                        upload_feedback = "{0}{1}<p>There were {2} lines of Jinja2 comments</p>".format(
                            upload_feedback,linesep,len(template_comment_namedtuples))
                        upload_feedback = template_numbered_dict_bothfixed(
                            upload_feedback,suffix=None,uniq=False)

                    """ Now we are going to write the file without any alteration and
                    read it in again, just so Python will instruct us about what newlines are
                    """
                    fpath_raw = "{0}.raw".format(template_filepath)
                    with open(fpath_raw, 'wb') as outraw:
                        outraw.write(fdata_raw)
                        # fdata_raw is the stored result of applying .read() method
                    sleep(0.5)
                    """ Note that the delay you see when /upload form is submitted is half second
                    caused by the deliberate sleep above rather than any slowness in bottle
                    """
                    with open(fpath_raw, 'U') as inf:
                        inf.readline()
                        template_newlines = inf.newlines
                    # newlines will always be singular as above just a single readline()
                        
                else:
                    upload_feedback = "Upload failed. Does {0} directory exist?".format(
                        DIR_UPLOAD_UNEXPANDED)
                    # End of failure feedback message

        else:
            upload_feedback = "This system is designed to accept Jinja2 uploads."
            upload_feedback = "{0} Retry with file ending .j2".format(upload_feedback)
            #upload_feedback = "{0} {1}".format(upload_feedback,ext)
    else:
        upload_feedback = "Upload failed. Did you miss a field?"

    if done_bytes:
        print "Successful upload of {0:d} bytes of file {1}".format(done_bytes,fname)
        # Above we print rather than setting upload_feedback ( reported to user directly )
        done_dict[fname] = done_bytes
    else:
        if upload_feedback is None:
            upload_feedback = "Upload failed!"
        done_dict[fname] = 0

    html_feedback = HTML_BODY_WRAPPER.format(upload_feedback)

    return html_feedback


@get('/upload')
def upload_form():
    html_form = HTML_BODY_WRAPPER.format(FORM_UPLOAD_TEMPLATE.format('upload'))
    return html_form


@get('/uploadvars')
def upload_form_report():
    html_form = HTML_BODY_WRAPPER.format(FORM_UPLOAD_TEMPLATE.format('uploadvars'))
    return html_form


@get('/uploadonly')
def upload_form_basic():
    html_form = HTML_BODY_WRAPPER.format(FORM_UPLOAD_TEMPLATE.format('uploadonly'))
    return html_form


@post('/uploadonly')
@post('/uploadvars')
def upload_noauto():

    port = request.forms.get('port')
    data = request.files.get('data')
    fname_field = None
    upload_feedback = None
    upload_bytes = 0
    done_bytes = 0
    port_default = None

    if request.get_header('Content-Length'):
        port_default = port
    if upload_feedback is None and data is not None:
        data_raw = data.file.read() # small files
        fname = filename_postprocessed(data.filename)
        if len(fname) < 2:
            return "Illegal filename!"
        else:
            upload_bytes = len(data_raw)

    if upload_bytes > 0:
        verbosity = 1
        if 'vars' in request['bottle.route'].rule:
            verbosity = 2
        upload_feedback = upload_returning_feedback(data,fname,upload_bytes,data_raw,verbosity)
        #upload_feedback = fields_list()
        if verbosity > 1 and 'diva_text' in fields_list():
            upload_feedback = template(data_raw,diva_text='Plavalaguna')
        
    return upload_feedback


@post('/upload')
def upload_autoquestion():
    global template_prompt_remove_list

    port = request.forms.get('port')
    data = request.files.get('data')
    fname_field = None
    upload_feedback = None
    upload_bytes = 0
    done_bytes = 0
    port_default = None

    if request.get_header('Content-Length'):
        port_default = port
    if upload_feedback is None and data is not None:
        data_raw = data.file.read() # small files
        fname = filename_postprocessed(data.filename)
        if len(fname) < 2:
            return HTML_BODY_WRAPPER.format('Illegal filename!')
        else:
            upload_bytes = len(data_raw)

    flist = []
    if upload_bytes > 0:
        #if 'vars' in request['bottle.route'].rule:
        upload_feedback = upload_returning_feedback(data,fname,upload_bytes,data_raw,1)
        # In next call setting the second parameter True will ensure unique list returned
        flist = fields_list(False,True)
        """
        if verbosity > 1 and 'diva_text' in flist:
            upload_feedback = template(data_raw,diva_text='Plavalaguna')
        """
    else:
        return HTML_BODY_WRAPPER.format('Zero bytes uploaded!')

    # tuple4 = (fname,linenum,linefieldnum,field_prompt)

    form_looped_list = []

    field_name_set = set()
    for tup4 in tuple4list():
        fname = tup4[0]
        if fname in field_name_set:
            continue
        field_prompt = tup4[3]
        if field_prompt is None:
            field_prompt = fname
        else:
            linenum1 = -1+tup4[1]
            template_prompt_remove_list.append(linenum1)
        #label_commented = FORM_INPUT_LABEL_ZERO_ID.format(fname,fname,field_prompt)
        label_commented = FORM_INPUT_LABEL_ZERO_ID4.format(fname,fname,field_prompt,fname)
        form_looped_list.append(label_commented)
        form_looped_list.append(FORM_INPUT_ZERO_ID_BR.format(fname))
        field_name_set.add(fname)

    form_looped = (linesep).join(form_looped_list)

    raw_form = "{0}{1}{2}{1}{3}{4}".format(FORM_ACTION_LINE.format('raw'),
                                           linesep,form_looped,
                                           FORM_SUBMIT_BUTTON.format('Submit values'),
                                           '</form>')

    html_form = HTML_BODY_WRAPPER.format(raw_form)
    return html_form


@post('/autoquestionans')
@post('/subbed')
@post('/raw')
def raw_substituted():
    #sub_dict = defaultdict(lambda: '[unknown]')
    sub_dict = request.forms
    response.content_type = 'text/plain'
    #if len(template_prompt_remove_list) == 0:
    if len(template_comment_linenum_list) == 0:
        rawsub = template(template_data,sub_dict)
    else:
        template_data_filtered_list = []
        for idx,line in enumerate(template_data.split(template_newlines)):
            if idx in template_comment_linenum_list:
                continue
            template_data_filtered_list.append(line)

        template_data_filtered = (template_newlines).join(template_data_filtered_list)
        rawsub = template(template_data_filtered,sub_dict)
    return rawsub


@get('/commentcount')
def fields_namedtuple_list_length():
    return HTML_BODY_WRAPPER_NL.format(len(template_comment_namedtuples))


@get('/comments')
def fields_namedtuple_list_html():
    comments_list = []
    for com in template_comment_namedtuples:
        comments_list.append(LINE_COMMENT_WITH_NUMBER_P.format(com.linenum,com.text))
    comments_markedup = HTML_BODY_WRAPPER_NL.format(''.join(comments_list))
    return comments_markedup


run(host='localhost', port=8000, reloader=True)
