#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from bottle import abort, error, get, post, redirect, response, request
from bottle import route, run, static_file, template
from collections import namedtuple
from os import linesep,path
import re
from string import printable
SET_PRINTABLE = set(printable)
DIR_UPLOAD_UNEXPANDED = '~/j2upload'
""" Above UNEXPANDED is what will be presented in any feedback to prevent
unnecessary leakage of local path information
"""
DIR_UPLOAD = path.expanduser(DIR_UPLOAD_UNEXPANDED)
BYTES_MINIMUM = 5
RE3GROUP = re.compile('({{)(\s+\w*\s+)(}})')
""" Next template is for internal use and has a {0} placeholder that will be target of
a later substitution
"""
FORM_UPLOAD_TEMPLATE = """
        <form action="/{0}" method="post" enctype="multipart/form-data">
          <input type="text" name="name" />
          <input type="file" name="data" />
          <input type="submit" name="submit" value="Upload Template" />
        </form>
"""
HTML_BODY_WRAPPER = "<html><body>{0}</body></html>"

template_data = None
template_lines = []
template_dict = {}
template_numbered_dict = {}
template_field_count = 0

TemplateField = namedtuple('TemplateField', ['index','linenum','linefieldnum','name'])

@route('/')
@route('/index.html')
def index():
    return '<a href="/hello">Clickable link</a> to Hello nobody page'

@route('/hello')
def hello():
    return '<h1>hello nobody in particular</h1>'

@route('/hello/:name')
def hello_name(name):
    page = request.GET.get('page', '1')
    return "<h1>hello {0} <br/>({1})</h1>".format(name, page)

@route('/static/:filename')
def serve_static(filename):
    return static_file(filename, root=DIR_UPLOAD)

@route('/raise_error')
def raise_error():
    abort(404, "Aborted four oh four!")

@route('/redirect')
def redirect_to_hello():
    redirect('/hello')

@route('/ajax')
def ajax_response():
    return {'dictionary': 'Seeing ajax response? Content-Type will be "application/json"'}

@error(404)
def error404(error):
    return '404 error!'


def fields_namedtuple_list(sort=True):
    """ Return a list of namedtuples()
    For convenience I repeat the definition of TemplateField which has 4 fields
    first of which is a simple index (zero-based)
    TemplateField = namedtuple('TemplateField', ['index','linenum','linefieldnum','name'])
    """
    nt_list = []
    for idx,dnum in enumerate(sorted(template_numbered_dict)):
        field_name = template_numbered_dict[dnum]
        linenum = template_dict[field_name][0]
        linefieldnum = template_dict[field_name][1]
        nt_list.append(TemplateField(idx,linenum,linefieldnum,field_name))
    return nt_list


def fields_list(sort=True):
    flist = [ field.name for field in fields_namedtuple_list(sort) ] 
    print flist
    return flist


def filename_postprocessed(filename):
    fname_pp = []
    filename_unicode = filename.encode('utf8','ignore')
    fname_ascii = filename_unicode.encode('ascii','ignore')
    if not set(fname_ascii).issubset(SET_PRINTABLE):
        return fname_pp
    fname_len = len(fname_ascii)
    if fname_len < 2:
        return fname_pp
    if '}' in fname_ascii and ';' in fname_ascii:
        return fname_pp
    #from collections import Counter
    dots = fname_ascii.count(chr(46))
    if dots > 1 and '..' in fname_ascii:
        return fname_pp
    if '%2' in fname_ascii:
        return fname_pp
    if 'chr(' in fname_ascii:
        return fname_pp
    fname_pp = fname_ascii
    return fname_pp


def upload_jinja2(fdata,fname,fbytes,fpart1,fext):
    global template_data, template_lines, template_dict, template_numbered_dict, template_field_count
    template_data = fdata
    written_bytes = -1
    fpath = "{0}/{1}".format(DIR_UPLOAD,fname)
    template_lines = []
    template_dict = {}
    template_numbered_dict = {}
    template_field_count = 0
    try:
        with open(fpath, 'wt') as outf:
            for idx,line in enumerate(fdata.split(linesep)):
                template_lines.append(line)
                written_bytes = fbytes
                outf.write("{0}{1}".format(line,linesep))
                field_index = 0
                for mat in RE3GROUP.finditer(line):
                    template_field_count += 1
                    field_name = mat.group(2).strip()
                    num_string = "{0:d}.{1:d}".format(idx,field_index)
                    num = -1
                    try:
                        num = float(num_string)
                    except:
                        num = 0
                    if num > -1:
                        #print "Added an entry to template_numbered_dict!"
                        template_numbered_dict[num] = field_name
                        template_dict[field_name] = [idx,field_index]
                        field_index += 1
            written_bytes = fbytes
    except IOError:
        print "IOError. Does {0} directory exist?".format(DIR_UPLOAD_UNEXPANDED)
        written_bytes = 0
    except:
        print "Error during upload to directory {0}".format(DIR_UPLOAD_UNEXPANDED)
        raise
        written_bytes = 0

    return written_bytes


def feedback_of_fields():
    """ Build a string of lines reporting on field information.
    For convenience I repeat the definition of TemplateField which has 4 fields
    first of which is a simple index (zero-based)
    TemplateField = namedtuple('TemplateField', ['index','linenum','linefieldnum','name'])
    """
    fields_named_list = fields_namedtuple_list(True)
    feedback_list = []
    for field in fields_named_list:
        fline = "Field {0} on line {1} is field {2} for that line (with index={3}).".format(
            field.name,field.linenum,field.linefieldnum,field.index)
        feedback_list.append(fline)
    return feedback_list


def feedback_of_fields_newlined():
    """ wrapper around feedback of fields which gives list which we then join. """
    return (linesep).join(feedback_of_fields())


def feedback_of_fields_br(newline_replacement='<br />'):
    """ wrapper around feedback of fields which replaces newlines with
    the newline_replacement.
    """
    feedback = feedback_of_fields_newlined()
    feedback_br = feedback.replace(linesep,newline_replacement)
    return feedback_br


def feedback_of_fields_paragraphed():
    """ wrapper around feedback of fields which splits on newline
    and uses html paragraphing instead.
    """
    feedback_list = feedback_of_fields()
    feedback_p = (''.join('<p>' + item + '</p>' for item in feedback_list))
    return feedback_p


def upload_returning_feedback(fdata,fname,upload_bytes,fdata_raw,verbosity=1):
    if upload_bytes:
        part1, ext = path.splitext(fname)
        if ext == '.j2':
            if upload_bytes > BYTES_MINIMUM:
                done_bytes = upload_jinja2(fdata_raw,fname,upload_bytes,part1,ext)
                if done_bytes > BYTES_MINIMUM:
                    upload_feedback = "Successful upload of {0:d} bytes of file {1}".format(
                        done_bytes,fname)
                    if verbosity > 0:
                        upload_feedback = "{0}. Template has {1} lines and {2} variables".format(
                            upload_feedback,len(template_lines),template_field_count)
                    if verbosity > 1:
                        upload_feedback = "{0}.{1}{2}".format(
                            upload_feedback,linesep,feedback_of_fields_paragraphed())
                else:
                    upload_feedback = "Upload failed. Does {0} directory exist?".format(
                        DIR_UPLOAD_UNEXPANDED)
                    # End of failure feedback message
        else:
            upload_feedback = "This system is designed to accept Jinja2 uploads."
            upload_feedback = "{0} Retry with file ending .j2".format(upload_feedback)
            #upload_feedback = "{0} {1}".format(upload_feedback,ext)
    else:
        upload_feedback = "Upload failed. Did you miss a field?"

    if done_bytes:
        print "Successful upload of {0:d} bytes of file {1}".format(done_bytes,fname)
        # Above we print rather than setting upload_feedback ( reported to user directly )
    elif upload_feedback is None:
        upload_feedback = "Upload failed!"
    else:
        pass

    html_feedback = HTML_BODY_WRAPPER.format(upload_feedback)

    return html_feedback


@get('/upload')
def upload_view():
    html_form = FORM_UPLOAD_TEMPLATE.format('upload')
    return html_form


@get('/uploadvars')
def upload_view():
    html_form = FORM_UPLOAD_TEMPLATE.format('uploadvars')
    return html_form


@post('/upload')
@post('/uploadvars')
def do_upload():
    name = request.forms.get('name')
    data = request.files.get('data')
    fname_field = None
    upload_feedback = None
    upload_bytes = 0
    done_bytes = 0

    if request.get_header('Content-Length'):
        fname_field = name
    if fname_field is None:
        upload_feedback = 'Upload failed - bad request'
    if upload_feedback is None and data is not None:
        data_raw = data.file.read() # small files
        fname = filename_postprocessed(data.filename)
        if len(fname) < 2:
            return "Illegal filename!"
        else:
            upload_bytes = len(data_raw)

    if upload_bytes > 0:
        verbosity = 1
        if 'vars' in request['bottle.route'].rule:
            verbosity = 2
        upload_feedback = upload_returning_feedback(data,fname,upload_bytes,data_raw,verbosity)
        upload_feedback = fields_list()
        if verbosity > 1 and 'diva_text' in fields_list():
            upload_feedback = template(data_raw,diva_text='Plavalaguna')
        
    return upload_feedback


@route('/tpl')
def tpl():
    return template('test template string')

run(host='localhost', port=8000, reloader=True)
