# Upload a template and interpret some Jinja2 variables

~~~
mkdir ~/j2upload
~~~

## Start the bottle webserver for the 'simple' example

~~~
python ./file_upload_template_simple.py
~~~

## Copy then upload the file single_diva_assoc.j2

~~~
cp single_diva_assoc.j2 /tmp/
~~~

## Example 1 at suffix /upload

Visit [upload page on local webserver](http://127.0.0.1:8000/upload)

Select by 'Browse' the file /tmp/single_diva_assoc.j2

Next to browse you will see 'single_diva_assoc.j2' when properly selected

Click 'Upload template'

The page should clear and display 'diva_text'

What you are seeing is the name of the [jinja2] variable extracted from the .j2 file you uploaded


## Example 2 at suffix /uploadvars

Visit [upload page on local webserver](http://127.0.0.1:8000/uploadvars)

Select by 'Browse' the file /tmp/single_diva_assoc.j2

Next to browse you will see 'single_diva_assoc.j2' when properly selected

Click 'Upload template'

The page should clear and display the text shown on the next line

~~~
Word association: diva Plavalaguna
~~~

What you are seeing is the result of the template processing having substituted Plavalaguna for variable diva_text


## What does this show and what next

The examples above demonstrates simple detection of jinja2 variables and also shows an example of substitution.

The script file_upload_template_simple.py has the suffix simple because it is designed to support these example and not much more.

Further development will happen on the main script file_upload_template.py which it is planned will autogenerate questions.


## Example .j2 files in input/

Jinja2 expression evaluation can be triggered by {{ yourvariable }}

The file group1inbetween2.j2 is an example where a single line template contains two expressions for evaluation






